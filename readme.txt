Welcome to Gaugefy
------------------

In order to run Gaugefy within the Xcode PhoneGap project you need to compile the source. To do so follow these steps:

----------------------------------------
Prerequisite:
- node must be installed on your machine
----------------------------------------

1. Clone this repo onto your machine

2. In the Terminal, cd into the root of this repository and run the following to initialise the submodule

	git submodule init

3. Now run the following to pull down the submodule code

	git submodule update

4. Copy and paste the following command into the Terminal

	node r.js -o src/js/app.build.js

5. Assuming nothing goes wrong, you will find the freshly compiled code in the 'xcode/www' folder

6. You can now load up the Xcode project found in the xcode folder


Keeping the submodule up-to-date with changes from the origin
-------------------------------------------------------------

I will try to make sure this repo is always pointing to the latest submodule code so just run 'git pull' from the repo root.

If you don't think it is up-to-date then you can run the following,

	git submodule foreach git pull origin master