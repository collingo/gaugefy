describe("Guagefy", function() {
	
	describe("Guage Model", function() {
		
		beforeEach(function() {
		  this.testgauge = new Guage();
		});
		
		afterEach(function() {
		  delete this.testgauge;
		});
		
		it("should exist", function() {
			expect(typeof this.testgauge).toEqual("object");
		});

		it("should hold default values", function() {
			expect(this.testgauge.get("title")).toEqual("");
			// expect(this.testgauge.get("swatchStitch")).toEqual(0);
			// expect(this.testgauge.get("swatchRows")).toEqual(0);
			// expect(this.testgauge.get("swatchWidth")).toEqual(0);
			// expect(this.testgauge.get("swatchHeight")).toEqual(0);
			// expect(this.testgauge.get("yarn")).toEqual("");
			// expect(this.testgauge.get("needle")).toEqual("");
		});

		if("should save to local storage", function() {
			this.testgauge.set("title","localStorage test");
			this.testgauge.save();
			var restored = localstorage.test;
			expect(restored.get("title")).toEqual("localStorage test");
		});

	});
	
});